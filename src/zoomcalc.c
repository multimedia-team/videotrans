/*
Copyright (c) 2005-2007, Sven Berkvens-Matthijsse

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

* Neither the name of deKattenFabriek nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include	"config.h"

#include	<stdio.h>
#ifdef		HAVE_MATH_H
#include	<math.h>
#endif		/* HAVE_MATH_H */
#include	<string.h>
#include	<errno.h>
#include	<stdlib.h>

typedef	enum
{
	ZOOM_TYPE_LETTERBOX,
	ZOOM_TYPE_PANSCAN
} zoom_type_t;

typedef	enum
{
	OUTPUT_SHELL
} output_type_t;

typedef	enum
{
	CHOOSE_DVD_NONE,
	CHOOSE_DVD_PAL,
	CHOOSE_DVD_NTSC
} choose_dvd_t;

#define		SEEN_SX			(1 << 0)
#define		SEEN_SY			(1 << 1)
#define		SEEN_SAX		(1 << 2)
#define		SEEN_SAY		(1 << 3)
#define		SEEN_DX			(1 << 4)
#define		SEEN_DY			(1 << 5)
#define		SEEN_DAX		(1 << 6)
#define		SEEN_DAY		(1 << 7)
#define		SEEN_ZOOM_TYPE		(1 << 8)
#define		SEEN_OUTPUT_TYPE	(1 << 9)
#define		SEEN_CHOOSE_DVD		(1 << 10)

/* Name:	main
 *
 * Description:	Main entrance to the program.
 *
 * Input:	argc		- the number of arguments
 *		argv		- the array of arguments
 *
 * Output:	Standard exit code.
 *
 * Side effects:None.
 */

int
main(const int argc, char * const * const argv)
{
	double		sx, sy, sax, say, dx, dy, dax, day, cx, cy, zx, zy;
	zoom_type_t	zoom_type;
	output_type_t	output_type;
	choose_dvd_t	choose_dvd;
	unsigned int	seen;
	int		i;
	char		*end;

	/* Defaults.
	 */

	seen = 0;
	zoom_type = ZOOM_TYPE_LETTERBOX;
	output_type = OUTPUT_SHELL;
	choose_dvd = CHOOSE_DVD_NONE;
	sx = sy = dx = dy = (double)0.0;
	sax = say = dax = day = (double)0.0;

	/* Parse the command line parameters.
	 */

	for (i = 1; i < argc; i++)
	{
#define		OPTION(var, string, bit)				\
		if (!strcmp(argv[i], (string)))				\
		{							\
			if (seen & (bit))				\
			{						\
				(void)fprintf(stderr, "You have already "\
					"specified the option %s\n",	\
					(string));			\
				exit(1);				\
			}						\
			seen |= (bit);					\
			if (i + 1 >= argc)				\
			{						\
				(void)fprintf(stderr, "Missing argument "\
					"for option %s\n", (string));	\
				exit(1);				\
			}						\
			i++;						\
			errno = 0;					\
			(var) = strtod(argv[i], &end);			\
			if (errno || (end == argv[i]) || *end)		\
			{						\
				(void)fprintf(stderr, "The value <%s> "	\
					"is illegal for the option %s\n",\
					argv[i], (string));		\
				exit(1);				\
			}						\
		}

		OPTION(sx, "-sx", SEEN_SX)
		else OPTION(sy, "-sy", SEEN_SY)
		else OPTION(sax, "-sax", SEEN_SAX)
		else OPTION(say, "-say", SEEN_SAY)
		else OPTION(dx, "-dx", SEEN_DX)
		else OPTION(dy, "-dy", SEEN_DY)
		else OPTION(dax, "-dax", SEEN_DAX)
		else OPTION(day, "-day", SEEN_DAY)
		else if (!strcmp(argv[i], "-panscan"))
		{
			if (seen & SEEN_ZOOM_TYPE)
			{
				(void)fprintf(stderr, "You have already "
					"specified a zoom type\n");
				exit(1);
			}
			seen |= SEEN_ZOOM_TYPE;
			zoom_type = ZOOM_TYPE_PANSCAN;
		}
		else if (!strcmp(argv[i], "-letterbox"))
		{
			if (seen & SEEN_ZOOM_TYPE)
			{
				(void)fprintf(stderr, "You have already "
					"specified a zoom type\n");
				exit(1);
			}
			seen |= SEEN_ZOOM_TYPE;
			zoom_type = ZOOM_TYPE_LETTERBOX;
		}
		else if (!strcmp(argv[i], "-shell"))
		{
			if (seen & SEEN_OUTPUT_TYPE)
			{
				(void)fprintf(stderr, "You have already "
					"specified an output type\n");
				exit(1);
			}
			seen |= SEEN_OUTPUT_TYPE;
			output_type = OUTPUT_SHELL;
		}
		else if (!strcmp(argv[i], "-choosedvd"))
		{
			if (seen & SEEN_CHOOSE_DVD)
			{
				(void)fprintf(stderr, "You have already "
					"specified the option -choosedvd\n");
				exit(1);
			}
			seen |= SEEN_CHOOSE_DVD;
			if (i + 1 >= argc)
			{
				(void)fprintf(stderr, "Missing argument "
					"for option -choosedvd\n");
				exit(1);
			}
			i++;
			if (!strcmp(argv[i], "pal"))
				choose_dvd = CHOOSE_DVD_PAL;
			else if (!strcmp(argv[i], "ntsc"))
				choose_dvd = CHOOSE_DVD_NTSC;
			else
			{
				(void)fprintf(stderr, "Illegal argument "
					"for option -choosedvd (should be "
					"either 'pal' or 'ntsc')\n");
				exit(1);
			}
		}
		else
		{
			(void)fprintf(stderr, "You have specified an unknown "
				"option <%s>\n", argv[i]);
			exit(1);
		}
	}

	/* Check whether we have the options that we need.
	 */

#define		MISSING(var, string, bit)				\
	if (!(seen & (bit)))						\
	{								\
		(void)fprintf(stderr, "You have not specified the %s option\n",\
			(string));					\
		exit(1);						\
	}								\
	if ((var) < (double)1.0)					\
	{								\
		(void)fprintf(stderr, "Illegal value for %s specified\n", \
			(string));					\
		exit(1);						\
	}

	MISSING(sx, "-sx", SEEN_SX);
	MISSING(sy, "-sy", SEEN_SY);

	/* Check for aspects.
	 */

	if ((seen & SEEN_SAX) && !(seen & SEEN_SAY))
	{
		(void)fprintf(stderr, "You have specified -sax but not -say\n");
		exit(1);
	}
	if ((seen & SEEN_SAY) && !(seen & SEEN_SAX))
	{
		(void)fprintf(stderr, "You have specified -say but not -sax\n");
		exit(1);
	}
	if (!(seen & SEEN_SAX) && !(seen & SEEN_SAY))
	{
		/* Assume square pixels.
		 */

		sax = sx;
		say = sy;
	}

	/* Calculate stuff for the destination size.
	 */

	if (CHOOSE_DVD_NONE == choose_dvd)
	{
		/* The user has to specify the destination size if -choosedvd
		 * is not used.
		 */

		MISSING(dx, "-dx", SEEN_DX);
		MISSING(dy, "-dy", SEEN_DY);
	}
	else if (!(seen & SEEN_DX) && !(seen & SEEN_DY))
	{
		/* The user wants us to choose a resolution for them.
		 */

		if (CHOOSE_DVD_PAL == choose_dvd)
		{
			if (sx > 384)
			{
				dx = 720;
				dy = 576;
			}
			else if (sy > 320)
			{
				dx = 352;
				dy = 576;
			}
			else
			{
				dx = 352;
				dy = 288;
			}
		}
		else /* if (CHOOSE_DVD_NTSC == choose_dvd) */
		{
			if (sx > 384)
			{
				dx = 720;
				dy = 480;
			}
			else if (sy > 288)
			{
				dx = 352;
				dy = 480;
			}
			else
			{
				dx = 352;
				dy = 240;
			}
		}
	}
	else /* if ((seen & SEEN_DX) || (seen & SEEN_DY)) */
	{
		if ((seen & SEEN_DX) && !(seen & SEEN_DY))
		{
			(void)fprintf(stderr,
				"You have specified -dx but not -dy\n");
			exit(1);
		}
		if ((seen & SEEN_DY) && !(seen & SEEN_DX))
		{
			(void)fprintf(stderr,
				"You have specified -dy but not -dx\n");
			exit(1);
		}
	}

	/* Destination aspect calculations.
	 */

	if ((seen & SEEN_DAX) && !(seen & SEEN_DAY))
	{
		(void)fprintf(stderr, "You have specified -dax but not -day\n");
		exit(1);
	}
	if ((seen & SEEN_DAY) && !(seen & SEEN_DAX))
	{
		(void)fprintf(stderr, "You have specified -day but not -dax\n");
		exit(1);
	}
	if (!(seen & SEEN_DAX) && !(seen & SEEN_DAY))
	{
		if (CHOOSE_DVD_NONE == choose_dvd)
		{
			/* Assume square pixels.
			 */

			dax = dx;
			day = dy;
		}
		else
		{
			/* Choose either 4:3 or 16:9, depending on the source
			 * aspect ratio.
			 */

			if (sax / say < 1.4)
			{
				dax = 4;
				day = 3;
			}
			else
			{
				dax = 16;
				day = 9;
			}
		}
	}

	/* Are we letterboxing or panscanning?
	 */

	if (ZOOM_TYPE_LETTERBOX == zoom_type)
	{
		/* Always use the entire source image when letterboxing.
	 	 */

		cx = sx;
		cy = sy;

		/* Would the picture at full size be too tall or too wide?
		 */

		if ((sax / say) < (dax / day))
		{
			/* The picture would be too narrow.
			 */

			zx = ((day * sax) / (dax * say)) * dx;
			zy = dy;
		}
		else
		{
			/* The picture would be too wide.
			 */

			zx = dx;
			zy = ((dax * say) / (day * sax)) * dy;
		}
	}
	else /* if (ZOOM_TYPE_PANSCAN == zoom_type) */
	{
		/* Always use the entire destination image.
		 */

		zx = dx;
		zy = dy;

		/* Would the picture at full size be too tall or too wide?
		 */

		if ((sax / say) < (dax / day))
		{
			/* The picture would be too tall.
			 */

			cx = sx;
			cy = ((sax * day) / (dax * say)) * sy;
		}
		else
		{
			/* The picture would be too tall.
			 */

			cx = ((dax * say) / (day * sax)) * sx;
			cy = sy;
		}
	}

	/* Round off the zoom size to 8 pixel.
	 */

	zx = floor((zx + 4.0) / 8.0) * 8.0;
	if (zx > dx)
		zx = dx;
	zy = floor((zy + 4.0) / 8.0) * 8.0;
	if (zy > dy)
		zy = dy;

	/* Print the output as wanted.
	 */

	if (OUTPUT_SHELL == output_type)
	{
		(void)printf("DX=%.0f; DY=%.0f; CX=%.0f; CY=%.0f; "
			"ZX=%.0f; ZY=%.0f; DAX=%.0f; DAY=%.0f\n",
			dx, dy, cx, cy, zx, zy, dax, day);
	}

	/* We're done.
	 */

	return 0;
}
